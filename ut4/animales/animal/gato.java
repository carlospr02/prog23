package animal;

public class gato extends animal
{
	private int vidas;
	private boolean castrado;
	
	public gato()
	{
		super();
		vidas=7;
	}
	
	public gato(String c,double p, int vidas)
	{
		super(p,c);
		this.vidas=vidas;
	}
	
	public gato(gato g)
	{
		super(g.peso,g.color);
		vidas = g.vidas;
	}
	
	public int getVidas()
	{
		return vidas;
	}

	public void setVidas(int v)
	{
		vidas=v;
	}
		
	public void mataGato()
	{
		if (vidas > 0)
			vidas--;
	}	
	
	public void castrar()
	{
		if(castrado)
			System.out.println("El gato ya esta castrado");
		else
		{
			castrado=true;
			System.out.println("El gato ha sido castrado con exito");
		}
	}
	
	@Override
	public String toString()
	{
		if(castrado)
			return "Vidas= "+vidas+" peso= "+peso+" color= "+color+" castrado= SI";
		else
			return "Vidas= "+vidas+" peso= "+peso+" color= "+color+" castrado= NO";
	}
}
