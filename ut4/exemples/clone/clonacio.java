//package figuras;

/**
 * Esta classe permet instanciar objectes corresponents a la figura geomètrica Rectangle
 * definida por un alt y un ample ...
 * @author Ricardo Cantó Abad
 *
 */
class Rectangle implements Cloneable {
	/**
	 * El valor del'ample expressat en cm.
	 */
	private double ample;
	/** 
	 * El valor del'alt expressat en cm
	 */
	private double alt;
	/**
	 * Constructor per defecte
	 */
	public Rectangle() {
		//super();
		ample=alt=1;
	}
	/**
	 * Constructor d'ús general ...
	 * @param ample El valor de l'ample en cm
	 * @param alt El valor de l'alt en cm
	 */
	public Rectangle(double ample, double alt) {
		//super();
		this.ample = ample;
		this.alt = alt;
	}
	/**
	 * Retorna el valor del'ample en cm.
	 * @return El'ample en cm.
	 */
	public double getample() {
		return ample;
	}
	/**
	 * Permet modificar "ample" en cm.
	 * @param ample El valor de l'ample en cm.
	 */
	public void setAmple(double ample) {
		this.ample = ample;
	}
	/**
	 * Retorna l'alt en cm.
	 * @return el valor de l'alt en cm
	 */
	public double getalt() {
		return alt;
	}
	/**
	 * Permet modificar l'alt ... 
	 * @param alt El nou valor d'alt
	 */
	public void setalt(double alt) {
		this.alt = alt;
	}
	//@Override
	/**
	 * Mètode que retorna el valor dels atributs
	 * @return Un text contenint els valors dels atributs
	 */
/*	public String toString() {
		return "Rectangle [ample=" + ample + ", alt=" + alt + "]";
	}*/
	/**
	 * Calcul l'área de la figura
	 * @return Àrea expressada en cm2
	 */
	public double area() { return ample*alt; }
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		if (this != null)
			return (Rectangle) super.clone();
		else
			return null;
	}
}

public class clonacio
{
	public static void main(String[] args) throws CloneNotSupportedException {
		Rectangle r1 = new Rectangle();
		Rectangle r2 = (Rectangle) r1.clone();
		System.out.println(r1);	// r1.toString()
		System.out.println(r2);	// r1.toString()
	}
}
