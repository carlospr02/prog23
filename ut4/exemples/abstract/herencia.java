// Programa que use les classes Base i derivada

import figures.Figura;
import figures.Rectangle;

// import figures.*;

public class herencia
{
	public static void main(String args[])
	{
		Figura f1 = new Figura("verd");
		Figura f2 = new Figura();	// figura negra
		Rectangle r1 = new Rectangle();	// de color negre i ample i alt iguals a 2 i 1, respectivament
		Rectangle r2 = new Rectangle("groc",3,4);
		
		System.out.println(f1);	// crida al toString
		System.out.println(f2);
		System.out.println(r1);
		System.out.println(r2);	
		// Rectangle no té definit mètodo setColor() però l'està heretant de la seua classe base Figura
		r1.setColor("blau");
		System.out.println(r1);	// System.out.println(r1.toString());
		
	}
}
