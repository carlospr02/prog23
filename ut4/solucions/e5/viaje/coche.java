package viaje;

public class coche extends vehiculo
{
    private int puertas;

    public coche(double consumo, double potencia, int puertas)
    {
        super(consumo,potencia);

        if(puertas<0)
            this.puertas=puertas*-1;
        /*else
            this.puertas=puertas;*/
    }

    public void setPuertas(int puertas)
    {
        this.puertas = puertas;
    }

    public int getPuertas()
    {
        return puertas;
    }

    public String toString()
    {
        return "El coche con "+puertas+" puertas, tiene una potencia de "+potencia+" y un consumo de "+consumo+" litros por km";
    }
    @Override
    public double consumViatge(double km)
    {
        //por cada km se cobran 20€
        return km*30;
    }
}
