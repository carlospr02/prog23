
// classe base ABSTRACTA
abstract class Vehicle
{
	// els atributs han de ser protected en les classes base
	protected int rodes;
	protected double velocitat;
	protected final double velMax;
	
	public Vehicle () { rodes = 4; velocitat = 0; velMax = 140; }
	public Vehicle (int r, double v, double vm) {
	 rodes = r; 
	 velocitat = v;
	 if (vm >= 0)
	 	velMax = vm;
	 else
	 	velMax = 0;
	 limitarVelocitat();
	} 
	public void limitarVelocitat()
	{	
	 if (velocitat < 0)
		 velocitat = 0;
	 if (velocitat > velMax)
		velocitat = velMax;
	}
	// accelerar
	public void accelerar(double incVel)	// si incVel és negativa frenarà
	{
		velocitat += incVel;
		limitarVelocitat();
	}
	
	public void detindre() { velocitat = 0; }
	public void moure() { velocitat = 10; }
	public String toString() { return "rodes: " + rodes + ", velocitat: " + velocitat + ", velocitat máxima: " + velMax; }
}

class Bicicleta extends Vehicle
{
	private int marxes;	// private perque no té classes derivades
	public Bicicleta() { super(); marxes = 21; }
	public Bicicleta (int r, double v, double vm, int m) 
	{ 
		super(r,v,vm);
		if (m >= 0)
			marxes = m;
		else
			marxes = 1;
	}
	public String toString() { return "rodes: " + rodes +  ", velocitat: " + velocitat + ", velocitat máxima: " + velMax + ", marxes: " + marxes; }
}
// donat que esta classe conté un mètode abstracte, la classe també ha de ser abstracta
abstract class Motoritzat extends Vehicle
{
	public double potencia;	// en Caballs de vapor (CV)
	public Motoritzat() { super(); potencia = 30; }
	public Motoritzat(int r, double v, double vm, double p)
	{ 
		super(r,v,vm);
		if (p >= 0)
			potencia = p;
		else
			potencia = 30;
	}
	public String toString() { return "rodes: " + rodes + ", velocitat: " + velocitat + ", velocitat máxima: " + velMax + ", potencia: " + potencia; }
	// METODE NO ABSTRACTE
	public double getPotenciaReal() { return potencia; }	// en cv
}

class Motocicleta extends Motoritzat
{
	private String tipus;
	public Motocicleta() { super(); tipus = "urbana"; }
	public Motocicleta (int r, double v, double vm, double p, String t)
	{
		super(r,v,vm,p);
		tipus = t;
	}
	public String toString() { return "rodes: " + rodes + ", velocitat: " + velocitat + ", velocitat máxima: " + velMax + ", potencia: " + potencia + ", tipus: " + tipus; }
	public double getPotenciaReal()
	{ return potencia*1000/1.36; }	// en Watts
}

class Automobil extends Motoritzat
{
	private int portes;
	public Automobil() { super(); portes = 5;}
	public Automobil (int r, double v, double vm, double p, int pt)
	{
		super(r,v,vm,p);
		portes = pt;
	}
	public String toString() { return "rodes: " + rodes + ", velocitat: " + velocitat + ", velocitat máxima: " + velMax + ", potencia: " + potencia + ",portes: " + portes;}
	@Override
	public  double getPotenciaReal() { return potencia/1.36; }	// en KiloWatts
	public int getPortes() { return portes; }
} 

public class e2c
{
	public static void main(String args[])
	{
		// no puc crear vehicles per ser la seua classe abstracta
		//Vehicle v = new Vehicle(4,200,160);	// 4, 160, 160
		Bicicleta b = new Bicicleta(0,10,50,24);
		//Motoritzat m = new Motoritzat(4,100,150,56);
		Motocicleta mt = new Motocicleta(0,30,100,40,"trail");
		Automobil a = new Automobil(5,80,160, 60, 5);
		//System.out.println(v);
		System.out.println(b);
		//System.out.println(m);
		System.out.println(mt);
		System.out.println(a);
		System.out.println("La potència real en Watts de la motocicleta és " + mt.getPotenciaReal() );
		System.out.println("La potència real en KiloWatts de l'automòbil és " + a.getPotenciaReal() );
		
		Motoritzat ref = new Automobil();	// EXEMPLE D'UPCASTING
		int portes = ((Automobil) ref).getPortes();
		System.out.println("Té " + portes + " portes");
		double pot = ref.getPotenciaReal();	// és el mètode de la classe derivada, en kw. És un mètode sobreescrit en la classe derivada
		System.out.println("La potència és "+ pot);
		
		
		// La sobrecàrrega es fa entre mètodes d'igual nom en la mateixa classe, no entre classe base i derivada
		

	}
}
