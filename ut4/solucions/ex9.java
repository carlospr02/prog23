/*
   Realitza un programa en Java que faça el següent:
• llija un valor numèric des de teclat
• cree un objecte Double per al valor introduït amb el constructor que requereix com a paràmetre un
String
• obtinga el valor double (tipus bàsic) corresponent
• cree un segon objecte Double per al valor introduït amb el constructor que requereix com a
paràmetre el double obtingut anteriorment
• obtinga a partir del Double primer el valor sencer (int) més pròxim (arrodoniment) i el valor truncat
• obtinga el valor hexadecimal, octal i binari corresponent a l'enter truncat. **/

public class ex9 {

        public static void main (String args[]){
                if (args.length>0){
                    //cree un objeto Double para el valor introducido por teclado con el constructor que requiera como parámetro un String
                    Double d = Double.valueOf(args[0]); //lea un valor numérico desde teclado
                    //obtenga el valor double (tipo básico) correspondiente
                    double n = d.doubleValue();
                    //cree un segundo objeto Double para el valor introducido con el constructor que requiera como parámetro el double obtenido anteriormente
                    Double d2 = Double.valueOf(n);
                    /*
                        obtenga a partir del Double primero el valor entero (int) más próximo (redondeo) y el valor aproximado.
                    **/
                    //valor redondeado
                    int red = (int) Math.round(d);
                    System.out.println("Valor redondeado: "+red);
                    //valor truncado
                    int trunc = d.intValue();
                    System.out.println("Valor truncado: "+trunc);
                    /*
                        obtenga el valor hexadecimal, octal y binario del entero aproximado.
                    **/
                    Integer i = Integer.valueOf(red);
                    //Hexadecimal
                    System.out.println(i.toHexString(red));
                    //Octal
                    System.out.println(i.toOctalString(red));
                    //Binario
                    System.out.println(i.toBinaryString(red));

                }
                else
                    System.out.println("Forma de uso: java ej2 valorEntero");
        }
}
