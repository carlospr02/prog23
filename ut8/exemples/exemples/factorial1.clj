(defn factorial
	"obté el factorial de un enter passat como a paràmetre"
 	[n]
	(if (> n 1)
		(* n (factorial (- n 1)))
		1
	)
)

(println (factorial 5))
