// diccionari per a traduïr del valencià al castellà
import java.util.*;

public class exHashtable
{
	public static void main(String args[])
	{
		String val,cas;
		
		Scanner ent = new Scanner(System.in);
		Hashtable<String,String> dicc = new Hashtable<String,String>();
		System.out.println("Introduix paraula en valencià (enter per a acabar):");
		val = ent.nextLine();
		while (!val.isEmpty())	// mentre val no siga buïda
		{
			System.out.println("Introduix traducció en castellà:");
			cas = ent.nextLine();
			dicc.put(val,cas);
			System.out.println("Introduix paraula en valencià (enter per a acabar):");
			val = ent.nextLine();
		}
		// demane una paraula valenciana a l'usuari i la mostre traduïda al castellà
		System.out.println("Introduïx paraula en valencià per a traduïr-la");
		val = ent.nextLine();
		cas = dicc.get(val);
		if (cas != null)
			System.out.println("En castellà és " + cas);
		else
			System.out.println("No tinc eixa paraula al diccionari");
		System.out.println("Introduix paraula a esborrar");
		val = ent.nextLine();
		dicc.remove(val);
		// Torne a traduïr la paraula
		cas = dicc.get(val);
		if (cas != null)
			System.out.println("En castellà és " + cas);
		else
			System.out.println("No tinc eixa paraula al diccionari");	
			
		// Finalment mostrarem tot el diccionari
		Enumeration<String> claus = dicc.keys();
		Enumeration<String> elements = dicc.elements();
		while (claus.hasMoreElements() )
			System.out.println(claus.nextElement() + " en castellà vol dir " + elements.nextElement());
			
			
			
	}
		
}
