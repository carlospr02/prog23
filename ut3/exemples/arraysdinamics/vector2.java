//Demonstrate various Vector operations. 
import java.util.*; 

public class vector2
{ 
	public static void main(String args[])
	{ 
		// initial size is 3, increment is 2 
		Vector<Number> v = new Vector<Number>(3, 2); 
		System.out.println("Initial size: " + v.size()); 
		System.out.println("Initial capacity: " + v.capacity()); 
		/* v.add(new Integer(1)); */ v.add(1);
		//v.add(new Integer(2)); // FORMA OBSOLETA
		v.add(Integer.valueOf(2));
		v.add(new Integer(3)); 
		v.add(new Integer(4)); 
		System.out.println("Capacity after four additions: " + 
		v.capacity()); 
		v.add(new Double(5.45)); 
		System.out.println("Current capacity: " + v.capacity()); 
		v.add(new Double(6.08)); 
		v.add(new Integer(7)); 
		System.out.println("Current capacity: " + v.capacity()); 
		v.add(new Float(9.4)); 
		v.add(new Integer(10)); 
		System.out.println("Current capacity: " + v.capacity()); 
		v.add(new Integer(11)); 
		v.add(new Integer(12)); 
		System.out.println("First element: " + 	(Integer)v.firstElement()); 
		System.out.println("Last element: " + (Integer)v.lastElement()); 
		if(v.contains(new Integer(3)))  // if (v.contains(3))
			System.out.println("Vector contains a 3."); 
		// enumerate the elements in the vector. 
		Enumeration vEnum = v.elements(); 
		System.out.println("\nElements in vector:"); 
		while ( vEnum.hasMoreElements() ) 
			System.out.println( vEnum.nextElement()); 
		System.out.println(); 
	} 
}
