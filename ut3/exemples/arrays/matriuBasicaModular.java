// Exemple de MATRIU de tipus bàsic

public class matriuBasicaModular
{
	static final int F=2,C=3;	// constants per al nombre de files i columnes, com a atributs (accesibles a tota la classe)
	
	public static void main(String args[])
	{
		// primer creem l'array
		double matriu[][] = new double[F][C];
		
		carregaValors(matriu);	// simplement el nom de l'array
		mostraValors(matriu);
	}
	
	// funció que carrega valors en la matriu
	public static void carregaValors(double m[][])
	{
		for (int i = 0; i < F ; i++)	// i serà l'index de fila
			for (int j = 0; j < C ; j++)		// j serà l'index de columna
				m[i][j] = (int) (10*Math.random()) + 1;
	
	}
	// funció que mostra la matriu
	public static void mostraValors(double m[][])
	{
		for (int i = 0; i < F ; i++)
			{	// i serà l'index de fila
				for (int j = 0; j < C ; j++)		// j serà l'index de columna
					//System.out.println("Fila " + i + ", columna " + j + " : " + matriu[i][j]);
					System.out.print(m[i][j] + "\t");
				System.out.println("");
			}
	}
}

