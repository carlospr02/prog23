/*
Programa que llegirà del fitxer binari "nums.dat" (amb números de loteria) i mostrarà la mitjana, el màxim i el mínim */

import java.io.*;

public class exBinariLectura
{
	public static void main(String[] args) {
		File f = new File("nums.dat");
		if (f.exists())
		{
			int max,min, num, suma=0, cont=0; double mitjana;

			max=Integer.MIN_VALUE; min=Integer.MAX_VALUE;
			try(FileInputStream fis = new FileInputStream(f);
				DataInputStream dis = new DataInputStream(fis); )
			{
				
				while (true)
				{
					num = dis.readInt();
					System.out.println(num);
					if (num > max)
						max = num;
					if (num < min)
						min = num;
					suma += num;
					cont++;
				}
			}
			catch(EOFException e)
			{
				mitjana = suma/cont;
				System.out.println("El valor més alt ha sigut " + max + " i el més baix és " + min);
				System.out.println("La mitjana de tots els valors és " + mitjana);
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
		}
		else // NO EXISTEIX
			System.out.println("El fitxer no existeix");
		
		
	}
}