/*2. Programa que accepte com a paràmetre un fitxer de text i mostre quantes 
minúscules, majúscules i espais en blanc conté.*/

import java.io.*;
import java.util.*;

public class mesexercicis2{
	public static void main(String[] args) {
		if(args.length>0)
		{
			try(FileReader fr=new FileReader(args[0]); BufferedReader br= new BufferedReader(fr);)
			{
				//StringTokenizer st= new StringTokenizer(br.readLine());
				String s;
				int contMin=0,contMaj=0,contEsp=0;
				
				while((s=br.readLine())!= null)	// cada passada opera amb una línia del fitxer
				{
					for(int i=0; i<s.length();i++)	// cada passada opera amb un caracter
					{
						if(s.charAt(i)>='a' && s.charAt(i)<='z')
							contMin++;
						
						if(s.charAt(i)>='A' && s.charAt(i)<='Z' )
							contMaj++;
						if(s.charAt(i)==' ')
							contEsp++;
					}
				}
				System.out.println("Hi ha "+contMin+" minúscules");
				System.out.println("Hi ha "+contMaj+" majúscules");
				System.out.println("Hi ha "+contEsp+" espais en blanc");			}
			
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
			/*catch(IndexOutOfBoundsException e)
			{

			}*/
		}
		else
			System.out.println("Métode d'ús del programa: nomDelPrograma /ruta/al/fitxer");
	}
}
