import java.io.*;
import java.util.*;

abstract class figura
{
	private String color;
	public figura(String c) { color = c; }
	public abstract double area();
	public String getColor() { return color; }
}

class rectangle extends figura
{
	private double ample;
	private double alt;
	
	public rectangle(String c,double an,double al) { super(c); ample = an; alt = al; }
	public double area() { return ample*alt; }
}

public class exCollectionStream
{
	public static void main(String args[])
	{
		Vector<figura> figures = new Vector<figura>();
		rectangle r1 = new rectangle("verd",2,3);
		rectangle r2 = new rectangle("verd",3,4);
		rectangle r3 = new rectangle("blau",10,4);
		figures.add(r1); figures.add(r2); figures.add(r3);
		
		double suma= figures.stream()
			.filter(w -> w.getColor().equals("verd"))	// mètode intermedi
			.mapToDouble(w -> w.area())			// mètode intermedi
			.sum();						// mètode final
		System.out.println("La suma de les àrees dels rectangles que són verds és " + suma);
		
		
	}
}	
