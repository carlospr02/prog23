import java.io.*;
import java.util.Scanner;
//para cada pelicula voy a meter el titulo, directo y año
public class ex11
{
	public static void main(String[] args) 
	{
		Scanner ent = new Scanner(System.in);//creo scaner
		do{
		System.out.println("\n1.Alta\n2.Consultar pelicula\n3.Listar todas las películas\n4.Borra película\n0.Salir");
		int n=ent.nextInt();
		switch(n)
		{
			case 1: alta();break;
			case 2: System.out.println("Introduce el codigo");
			n=ent.nextInt();
			consultar(n);break;
			case 3: listar();break;
			case 4:System.out.println("Introduce el codigo");
			n=ent.nextInt();
			eliminar(n);break;
			default: System.exit(0);
		}
	}
	while(true);

	}
	public static void alta()
	{
		int ano;
		String titulo;
		String director;
		int cod;
		try(FileOutputStream fos = new FileOutputStream("peliculas.dat",true);
			DataOutputStream dos= new DataOutputStream(fos);
			)
		{
			Scanner ent = new Scanner(System.in);//creo scaner
			System.out.println("Escribe el codigo de la película: ");
			cod=ent.nextInt();
			ent.nextLine();
			System.out.println("Escribe el nombre de la película: ");
			titulo= ent.nextLine();
			System.out.println("Escribe el nombre del director");
			director=ent.nextLine();
			System.out.println("Escribe el año de la pelicula");
			ano= ent.nextInt();
			dos.writeInt(cod);dos.writeUTF(titulo);dos.writeUTF(director);dos.writeInt(ano);
		}
		catch(IOException e)
		{
			System.err.println(e.getMessage());
		}
	}
		
		public static void consultar(int codigo)
		{
			int cod;String titulo; String director; int ano;
			try(FileInputStream fis = new FileInputStream("peliculas.dat");
				DataInputStream dis = new DataInputStream(fis);)
			{
				 cod = dis.readInt();
				 titulo=dis.readUTF(); director=dis.readUTF();
				 ano=dis.readInt();
				 while( cod != codigo)
				 {
				 	cod=dis.readInt();
					 titulo=dis.readUTF(); director=dis.readUTF();
					 ano=dis.readInt();
				 }
				 if(cod == codigo)
				 	System.out.println("\nTitulo: "+titulo+"\nDirector "+director+"\nCodigo"+cod+"\nAño"+ano);

			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
			
		}
		
		public static void eliminar(int codigo)
		{
		int cod;String titulo; String director; int ano;
		File f1 = new File("peliculas.dat");
		File f2 = new File("peliculas2.dat");
			try(FileInputStream fis = new FileInputStream("peliculas.dat");
				DataInputStream dis = new DataInputStream(fis);
				FileOutputStream fos = new FileOutputStream("peliculas2.dat");
			DataOutputStream dos= new DataOutputStream(fos))
			{
				cod=dis.readInt();
			 titulo=dis.readUTF(); director=dis.readUTF();
			 ano=dis.readInt();
				while(true)			 
			{
			 	if(cod != codigo)
			 	{
			 		dos.writeInt(cod);
			 dos.writeUTF(titulo);dos.writeUTF(director);
			 dos.writeInt(ano);
			 	}
			 cod=dis.readInt();
			 titulo=dis.readUTF(); director=dis.readUTF();
			 ano=dis.readInt();
			 }
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
			f2.renameTo(f1);
		}
		
		public static void listar()
		{
			int cod;String titulo;String director;int ano;
			try(FileInputStream fis = new FileInputStream("peliculas.dat");
				DataInputStream dis = new DataInputStream(fis);)
			{
				while(true)
				{cod=dis.readInt();titulo=dis.readUTF();
								director=dis.readUTF();ano=dis.readInt();
			 System.out.println("Titulo: "+titulo+"\nDirector "+director+"\nCodigo "+cod+"\nAño"+ano+"\n");
				}
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
		}

}
