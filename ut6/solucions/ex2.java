//2. Realitza una aplicació que genere un número aleatori en fer clic sobre un botó. El nombre de dígits que tindrà 
//l'aleatori el prendrà d'un requadre de text (JTextField). Si el requadre té el valor 3, per exemple, 
//generarà aleatoris entre 0 i 999. Mostrarà el valor generat en una etiqueta.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Random;
import java.io.*;

public class ex2
{

	public static void main(String[] args) 
	{
		JFrame jf = new JFrame("Numero aleatorio");
		//CARACTERISTICAS DEL FRAME
		jf.setSize(1180, 200);
		jf.setLocationRelativeTo(null);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//JPanel y sus características
		JPanel jp = new JPanel();
		jp.setLayout(new GridLayout(2, 2, 10, 0));
		jf.add(jp);

		//creo el TextField y lo añado al panel
		JLabel jl0 = new JLabel("Digitos: "); jp.add(jl0);
		JTextField txtValor = new JTextField(); jp.add(txtValor);
		JButton jb = new JButton("Generar numero aleatorio (0-999)"); jp.add(jb);
		JLabel jl = new JLabel("Valor generado"); jp.add(jl);

		//jf.pack();
		jf.setVisible(true); //se añade al final

		
		ActionListener al = new ActionListener()
		{	// classe anònima
			int numAzar = 0;
			int valor = 0, potencia;
			public void actionPerformed(ActionEvent e)
			{	
				/*if (e.getSource() == jb)
				{*/
					valor = Integer.parseInt(txtValor.getText());
					potencia = (int) Math.pow(10,valor);
					numAzar = (int)(Math.random() * potencia); jl.setText(String.valueOf(numAzar));
				//}	

				try(FileWriter fw = new FileWriter("historicoSorteos.txt",true))
 				{
 					fw.write(String.valueOf(numAzar)+"\n");
 				}

 				catch (IOException e1)
 				{
 					System.err.println(e1.getMessage());
 				}

			}
		};
		jb.addActionListener(al);
		txtValor.addActionListener(al);

		//MEDIANTE EXPRESION LAMBDA
		/* 
		ActionListener al = e -> 
		{ 
			double numAzar = 0;
			int valor = 0;	
				if (e.getSource() == jb)
				{
					valor = Integer.parseInt(txtValor.getText());

					switch(valor)
					{
						default: jl.setText("Valor no valido"); break;
						case 1: numAzar = (int)(Math.random() * 10) + 1; jl.setText(String.valueOf((int)numAzar)); break;
						case 2: numAzar = (int)(Math.random() * 100) + 1; jl.setText(String.valueOf((int)numAzar)); break;
						case 3: numAzar = (int)(Math.random() * 1000) + 1; jl.setText(String.valueOf((int)numAzar));
					}
				}

				try(FileWriter fw = new FileWriter("historicoSorteos.txt",true))
 				{
 					fw.write(String.valueOf(numAzar)+"\n");
 				}

 				catch (IOException e1)
 				{
 					System.err.println(e1.getMessage());
 				}
		};
		jb.addActionListener(al);
		txtValor.addActionListener(al);*/
	}
}
