/* Programa que demane un enter corresponent a un dia de la setmana i indique el dia textual (1 a 5=Dia laborable, 6 a 7=cap de setmana) */

public class alternativaMultiple2
{
    public static void main(String args[])
    {
        int dia;

        System.out.println("Introduïx dia de la setmana (de 1 a 7):");
        dia = Integer.parseInt(System.console().readLine());
        //switch (dia%7)
        switch (dia)
        {
            case 1: 
            case 2: 
            case 3: 
            case 4: 
            case 5: System.out.println("Dia laborable");break;
            case 6: 
            case 7: System.out.println("Cap de setmana");break;
            default: System.out.println("Dia incorrecte");
        }

    }
}
