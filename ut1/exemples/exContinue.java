/* Exemple de continue per a forçar l'eixida de la passada actual del bucle

    Programa que demane números positius fins a acabar amb zero i mostre la suma dels positius ignorant els negatius */

public class exContinue
{
    public static void main(String args[])
    {
        double num, suma=0;

        do
        {    
           System.out.println("Introduix un número positiu (0 per a acabar):");
           num = Double.parseDouble(System.console().readLine());
           if (num < 0)
                continue;
           suma = suma + num; // suma += num; 
        } while (num != 0);   // mentre num siga diferent de zero
        System.out.println("La suma dels valors positius és " + suma);
    }
}
