// Transformació del for en while

public class for1While
{
    public static void main(String args[])
    {
        int cont = 0;
        while (cont < 100)
        {
            System.out.println(cont);
            cont = cont + 5; // cont += 5
        }
    }
}

