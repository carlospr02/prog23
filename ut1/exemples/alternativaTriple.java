// Exemple d'alternativa simple (if): programa que indicarà si el valor introduït és positiu

public class alternativaTriple
{
    public static void main(String args[])
    {
        double num;

        System.out.println("Introduïx un número:");
        num = Double.parseDouble(System.console().readLine());
        if ( num > 0) // NO PUNT I COMA
            System.out.println("És positiu");
        else    // sé que no és > 0, però tinc 2 opcions: negatiu o zero
            if (num < 0)
                System.out.println("És negatiu");
            else
                System.out.println("És zero");
    }   // tanca main
}   // tanca la classe
