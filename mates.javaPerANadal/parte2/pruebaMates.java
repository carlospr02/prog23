import java.util.*;

public class pruebaMates
{
	private final static int TAM = 10;
	private static Scanner ent = new Scanner(System.in);

	public static void main(String[] args)
	{
		
		Operando opd1, opd2;
		Operador opdr; int errores=0;
		Vector<Operando> vop1 = new Vector<Operando>();
		Vector<Operando> vop2 = new Vector<Operando>();
		Vector<Operador> vop = new Vector<Operador>();

		for (int i=0 ; i < TAM ; i++)
		{
			opd1 = new Operando();
			opd2 = new Operando();
			opdr = new Operador();
			// Si falla el cálculo se añade a los vectores correspondientes para volver a preguntar más tarde
			if (!pregunta(opd1,opd2,opdr))
			{
				vop1.add(opd1);
				vop2.add(opd2);
				vop.add(opdr);
				errores++;
			}
		}
		System.out.println("Tu calificación ha sido: " + (10 - errores));
		// Mientras queden operaciones por resolver
		while (!vop.isEmpty())
		{
			Iterator io1 = vop1.iterator();
			Iterator io2 = vop2.iterator();
			Iterator io = vop.iterator();
			while (io.hasNext())
			{
				opd1 = (Operando)io1.next();
				opd2 = (Operando)io2.next();
				opdr = (Operador)io.next();
				// Si acierta la pregunta se elimina de los vectores
				if (pregunta(opd1,opd2,opdr))
				{
					io1.remove();
					io2.remove();
					io.remove();
					errores--;
					System.out.println("Quedan " + errores);
				}
			}
		}	// fin bucle while		
	}

	public static boolean pregunta(Operando opd1,Operando opd2, Operador opdr)
	{
		System.out.println("\t" + opd1 + opdr + opd2 + " = ?");	
		int resp = ent.nextInt(), soluc=0;
		switch (opdr.getValor())
		{
			case '+': soluc = opd1.getValor() + opd2.getValor(); break;
			case '-': soluc = opd1.getValor() - opd2.getValor();break;
			case '*': soluc = opd1.getValor() * opd2.getValor();break;
			case '/': soluc = opd1.getValor() / opd2.getValor();break;
		}
		if ( resp == soluc )
		{
			System.out.println("BIEN, SIGUE ASÍ !!");
			return true;
		}
		System.out.println("INCORRECTO, REPASA LA OPERACIÓN ...");
		return false;
	}	
}
