docker pull mariadb

docker run --detach --name mariadb --env MARIADB_USER=root --env MARIADB_PASSWORD=root --env MARIADB_ROOT_PASSWORD=root mariadb:latest

docker pull phpmyadmin

docker start mariadb

docker run --name phpmyadminm -d --link mariadb:db -p 8088:80 phpmyadmin

(iniciar sessió amb phpmyadmin, port 8088, usuari root i contrasenya root)
