//9. Crea un programa visual que permeta incrementar el preu d'un disc, segons un determinat valor, indicant el seu identificador.
import java.sql.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ex9
{
	//DEFINICIÓ DE LES REFERÈNCIES COM A OBJECTES.
	private static JFrame jf; //Finestra principal del programa.
	private static JPanel jp1; //Primer panell del programa.
	private static JPanel jp11; //Primer panell del programa.
	private static JPanel jp12; //Primer panell del programa.
	private static JPanel jp2; //Segon panell del programa que contindrà botons.
	private static JLabel info; //Label de informació
	private static JLabel info2; //Label de informació
	private static JLabel lid; //Label de id
	private static JLabel laumento; //Label de l'augment
	private static JTextField jtfId; //Text field de id
	private static JTextField jtfAumento; //Text field de l'augment
	private static JButton jb; //Botó de aplicar.


	public static void main(String[] args)
	{
		//FINESTRA
			//Creem la finestra.
			jf = new JFrame("Augmenta Preu");
			//DEfinim un layout per a la finestra.
			jf.setLayout(new GridLayout(0,1));
			//Fem que el programa acabe al tancar la finestra.
			jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//Centrem la finestra.
			jf.setLocationRelativeTo(null);
			//Fem visible la finestra.
			jf.setVisible(true);

		//PANELL
			//Creem el panell amb un layout.
			jp1 = new JPanel(new GridLayout(1,0));
			jp11 = new JPanel(new GridLayout(0,1));
			jp12 = new JPanel(new GridLayout(0,1));
			jp2 = new JPanel(new GridLayout(0,1));
			//Afegim els panells a la finestra.
			jp1.add(jp11);
			jp1.add(jp12);
			jf.getContentPane().add(jp1);
			jf.getContentPane().add(jp2);

		//LABELS
			//Creem els labels.
			info = new JLabel("[Introdueix un ID i el augment a aplicar al preu]");
			info2 = new JLabel("");
			lid = new JLabel("ID");
			laumento = new JLabel("AUGMENT");
			//Afegim els labels als panells.
			jp11.add(lid);
			jp12.add(laumento);
			jp2.add(info);


		//TEXT FIELDS
			//Creem els JTextFields.
			jtfId = new JTextField();
			jtfAumento = new JTextField();
			//Afegim els elements als corresponents panells.
			jp11.add(jtfId);
			jp12.add(jtfAumento);


		//BOTÓ
			//creem el botó.
			jb = new JButton("AUGMENTAR PREU");
			//Agefim el botó al segón panell.
			jp2.add(jb);

		//CONTROL D'ESDEVENIMENTS
			//Creem els ActionListeners per al boto.
			ActionListener alb = e -> {accions(e);};
			//Afegim el ActionListener al botó.
			jb.addActionListener(alb);

		//EXTRAS
			//Fem que el tamany de la finestra s'adecue al minim dels elements.
			//jf.pack();
			//Posem un tamany mínim a la finestra.
			jf.setSize(400,200);


	}

	public static void accions(ActionEvent e)
	{
		String id,aumento,sql;
		id=jtfId.getText();
		aumento=jtfAumento.getText();
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/prova2","root","");
			Statement stmt = con.createStatement();)
		{
			System.out.println("Connecting Database...");
			sql="UPDATE discos SET PREU=PREU+"+aumento+" WHERE ID="+id+";";
			stmt.executeUpdate(sql);
			System.out.println(sql);
			System.out.println("Preu augmentat.");
			jtfId.setText("");
			jtfAumento.setText("");
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}

	}
}
