/*6. En un aparcament volen registrar les entrades de cotxes al llarg de cada dia. Per a això, has de fer un programa que treballe amb una classe Entrada que continga 2 atributs:
	- matrícula del cotxe (String)
	- instant de l'entrada (Rellotge)
Reutilitza la classe Rellotge de l'exercici 1.*/

import java.util.Scanner;

public class Entrada{ // ATENCION: PREVIAMENTE SE LLAMABA APARCAMENT

 
    private Rellotge r1 = null;
    private String matricula;
// constructor PER DEFECTE
    public Entrada(){ // ATENCION: EL NOMBRE DE LA CLASE QUE SE CREA (SUBCLASE) TIENE QUE SER EL MISMO QUE EL DE LA CLASE GENERAL DEL ARCHIVO. POR ESO SE HA CAMBIADO EL NOMBRE DE LA CLASE DEL ARCHIVO (PREVIAMENTE APARCAMENT) AL NOMBRE DE ENTRADA, PARA QUE COINCIDAN, SI NO, NO FUNCIONA Y PIDE QUE INSERTEMOS UN RETURN O ALGO ASI XD QUE TEXTO MAS LARGO
        r1 = new Rellotge();
        matricula = "1234AVC";
    }
    // CONSTRUCTOR GENERAL
        public Entrada(Rellotge r,String m){ // ATENCION: EL NOMBRE DE LA CLASE QUE SE CREA (SUBCLASE) TIENE QUE SER EL MISMO QUE EL DE LA CLASE GENERAL DEL ARCHIVO. POR ESO SE HA CAMBIADO EL NOMBRE DE LA CLASE DEL ARCHIVO (PREVIAMENTE APARCAMENT) AL NOMBRE DE ENTRADA, PARA QUE COINCIDAN, SI NO, NO FUNCIONA Y PIDE QUE INSERTEMOS UN RETURN O ALGO ASI XD QUE TEXTO MAS LARGO
        r1 = r;
        matricula = m;
    }
    public void mostraEntrada(){
        System.out.print("\n-----ENTRADA-----\nMatrícula del cotxe: " + matricula + "\nInstant de l'entrada: "); 
        r1.mostraRellotge();
    }
}
